package cn.nlte.science.units.time;

import cn.nlte.science.units.ValueWithUnit;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author yetao
 */
public class UTimeTest {
    
    public UTimeTest() {
    }

    @Test
    public void testConvert() {
        // 测试转化是否正确
        ValueWithUnit data = new ValueWithUnit(1, new UTime_h());
        assertEquals(1, data.getValue(),1E-6);
        data = data.convertTo(new UTime_day());
        assertEquals(0.04166667, data.getValue(),1E-6);
        data = data.convertTo(new UTime_ms());
        assertEquals(3600000, data.getValue(),1E-6);
        data = data.convertTo(new UTime_s());
        assertEquals(3600, data.getValue(),1E-6);
        data = data.convertTo(new UTime_us());
        assertEquals(3.6e+9, data.getValue(),1E-6);
        data = data.convertTo(new UTime_week());
        assertEquals(0.00595238, data.getValue(),1E-6);
    }

    @Test
    public void testAdd() {
        
    }

    @Test
    public void testSub() {
        
    }

    @Test
    public void testMultiply() {
        
    }

    @Test
    public void testDivide() {
        
    }
    
}
