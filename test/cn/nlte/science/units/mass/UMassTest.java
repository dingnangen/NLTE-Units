package cn.nlte.science.units.mass;

import cn.nlte.science.units.ValueWithUnit;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author yetao
 */
public class UMassTest {
    
    public UMassTest() {
    }

    @Test
    public void testConvert() {
        // 测试转化是否正确
        ValueWithUnit data = new ValueWithUnit(1, new UMass_Kg());
        assertEquals(1, data.getValue(),1E-10);
        data = data.convertTo(new UMass_g());
        assertEquals(1000, data.getValue(),1E-10);
        data = data.convertTo(new UMass_mg());
        assertEquals(1000000, data.getValue(),1E-10);
        data = data.convertTo(new UMass_t());
        assertEquals(0.001, data.getValue(),1E-10);
        data = data.convertTo(new UMass_q());
        assertEquals(0.01, data.getValue(),1E-10);
        data = data.convertTo(new UMass_dan());
        assertEquals(0.02, data.getValue(),1E-10);
        data = data.convertTo(new UMass_lb());
        assertEquals(2.2046226, data.getValue(),1E-6);
        data = data.convertTo(new UMass_oz());
        assertEquals(35.2739619, data.getValue(),1E-6);
        data = data.convertTo(new UMass_ct());
        assertEquals(5000, data.getValue(),1E-10);
        data = data.convertTo(new UMass_gr());
        assertEquals(15432.3583529, data.getValue(),1E-6);
        data = data.convertTo(new UMass_lt());
        assertEquals(0.0009842	, data.getValue(),1E-6);
        data = data.convertTo(new UMass_st());
        assertEquals(0.0011023, data.getValue(),1E-6);
        data = data.convertTo(new UMass_dr());
        assertEquals(564.3833912, data.getValue(),1E-6);
        data = data.convertTo(new UMass_jin());
        assertEquals(2, data.getValue(),1E-10);
        data = data.convertTo(new UMass_liang());
        assertEquals(20, data.getValue(),1E-10);
        data = data.convertTo(new UMass_qian());
        assertEquals(200, data.getValue(),1E-10);
    }

    @Test
    public void testAdd() {
        
    }

    @Test
    public void testSub() {
        
    }

    @Test
    public void testMultiply() {
        
    }

    @Test
    public void testDivide() {
        
    }
    
}
