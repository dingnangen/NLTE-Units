package cn.nlte.science.units;

/**
 * 单位单元类，封装单位类型及指数
 *
 * @author yetao
 */
public final class UnitElement {

    // 基本单位类型
    private BasicUnit unit;
    // 指数值
    private double exp;

    /**
     * 推荐的构造方法
     *
     * @param unit
     * @param exp
     */
    public UnitElement(BasicUnit unit, double exp) {
        this.unit = unit;
        this.exp = exp;
    }

    public UnitElement() {
    }

    /**
     * 实现对象的拷贝（不采用Java提供的）
     *
     * @return
     */
    public UnitElement copy() {
        return new UnitElement(unit, exp);
    }

    /**
     * 此处不重写Object的equal方法，太麻烦了...
     *
     * @param obj
     * @return
     */
    public boolean isEqual(UnitElement obj) {
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof UnitElement)) {
            return false;
        }
        UnitElement element = (UnitElement) obj;
        return (unit == element.getUnit()) && DoubleUtils.isSame(exp, element.getExp());
    }

    @Override
    public String toString() {
        return "UnitElement{" + "unit=" + unit + ", exp=" + exp + '}';
    }

    /**
     * @return the unit
     */
    public BasicUnit getUnit() {
        return unit;
    }

    /**
     * @param unit the unit to set
     */
    public void setUnit(BasicUnit unit) {
        this.unit = unit;
    }

    /**
     * @return the exp
     */
    public double getExp() {
        return exp;
    }

    /**
     * @param exp the exp to set
     */
    public void setExp(double exp) {
        this.exp = exp;
    }

}
