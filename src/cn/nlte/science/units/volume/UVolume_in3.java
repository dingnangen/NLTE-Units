package cn.nlte.science.units.volume;

import cn.nlte.science.units.BasicUnit;
import cn.nlte.science.units.DerivedUnit;
import cn.nlte.science.units.UnitElement;
import cn.nlte.science.units.length.ULength_in;
import java.util.ArrayList;
import java.util.List;

/**
 * 体积单位：in3，立方英寸
 *
 * @author yetao
 */
public class UVolume_in3 extends DerivedUnit {

    private final static List<UnitElement> elementList = new ArrayList<>();
    private final static double factor;
    private final static double additional = 0.0;
    private final static String description = "in3";

    static {
        factor = Math.pow(new ULength_in().getFactor(), 3.0);
        elementList.add(new UnitElement(BasicUnit.m, 3.0));
    }

    public UVolume_in3() {
        super(description, elementList, factor, additional);
    }

}
