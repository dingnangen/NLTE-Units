package cn.nlte.science.units.time;

import cn.nlte.science.units.BasicUnit;
import cn.nlte.science.units.DerivedUnit;
import cn.nlte.science.units.UnitElement;
import java.util.ArrayList;
import java.util.List;

/**
 * 时间单位：h，小时
 *
 * @author yetao
 */
public class UTime_h extends DerivedUnit {

    private final static List<UnitElement> elementList = new ArrayList<>();
    private final static double factor = 3600.0;
    private final static double additional = 0.0;
    private final static String description = "h";

    static {
        elementList.add(new UnitElement(BasicUnit.s, 1.0));
    }

    public UTime_h() {
        super(description, elementList, factor, additional);
    }

}
