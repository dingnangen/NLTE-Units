package cn.nlte.science.units.temperature;

import cn.nlte.science.units.BasicUnit;
import cn.nlte.science.units.DerivedUnit;
import cn.nlte.science.units.UnitElement;
import java.util.ArrayList;
import java.util.List;

/**
 * 温度单位：K，华氏温度
 *
 * @author yetao
 */
public class UTemperature_K extends DerivedUnit {

    private final static List<UnitElement> elementList = new ArrayList<>();
    private final static double factor = 1.0;
    private final static double additional = 0.0;
    private final static String description = "K";

    static {
        elementList.add(new UnitElement(BasicUnit.K, 1.0));
    }

    public UTemperature_K() {
        super(description, elementList, factor, additional);
    }

}
