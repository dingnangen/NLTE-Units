package cn.nlte.science.units.temperature;

import cn.nlte.science.units.BasicUnit;
import cn.nlte.science.units.DerivedUnit;
import cn.nlte.science.units.UnitElement;
import java.util.ArrayList;
import java.util.List;

/**
 * �¶ȵ�λ���棬�����¶�
 *
 * @author yetao
 */
public class UTemperature_C extends DerivedUnit {

    private final static List<UnitElement> elementList = new ArrayList<>();
    private final static double factor = 1.0;
    private final static double additional = 273.15;
    private final static String description = "C";

    static {
        elementList.add(new UnitElement(BasicUnit.K, 1.0));
    }

    public UTemperature_C() {
        super(description, elementList, factor, additional);
    }

}
