package cn.nlte.science.units.temperature;

import cn.nlte.science.units.BasicUnit;
import cn.nlte.science.units.DerivedUnit;
import cn.nlte.science.units.UnitElement;
import java.util.ArrayList;
import java.util.List;

/**
 * 温度单位：℉，华氏温度
 *
 * @author yetao
 */
public class UTemperature_F extends DerivedUnit {

    private final static List<UnitElement> elementList = new ArrayList<>();
    private final static double factor = 5.0 / 9.0;
    private final static double additional = 273.15 - 32.0 * 5.0 / 9.0;
    private final static String description = "F";

    static {
        elementList.add(new UnitElement(BasicUnit.K, 1.0));
    }

    public UTemperature_F() {
        super(description, elementList, factor, additional);
    }

}
