package cn.nlte.science.units.length;

import cn.nlte.science.units.BasicUnit;
import cn.nlte.science.units.DerivedUnit;
import cn.nlte.science.units.UnitElement;
import java.util.ArrayList;
import java.util.List;

/**
 * 长度单位：zhang，中国单位，1丈=10尺
 *
 * @author yetao
 */
public class ULength_zhang extends DerivedUnit {

    private final static List<UnitElement> elementList = new ArrayList<>();
    private final static double factor;
    private final static double additional = 0.0;
    private final static String description = "zhang";

    static {
        factor = new ULength_chi().getFactor() * 10.0;
        elementList.add(new UnitElement(BasicUnit.m, 1.0));
    }

    public ULength_zhang() {
        super(description, elementList, factor, additional);
    }

}
