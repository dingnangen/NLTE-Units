package cn.nlte.science.units.length;

import cn.nlte.science.units.BasicUnit;
import cn.nlte.science.units.DerivedUnit;
import cn.nlte.science.units.UnitElement;
import java.util.ArrayList;
import java.util.List;

/**
 * 长度单位：nmi，海里，1海里=1852米
 *
 * @author yetao
 */
public class ULength_nmi extends DerivedUnit {

    private final static List<UnitElement> elementList = new ArrayList<>();
    private final static double factor = 1852.0;
    private final static double additional = 0.0;
    private final static String description = "nmi";

    static {
        elementList.add(new UnitElement(BasicUnit.m, 1.0));
    }

    public ULength_nmi() {
        super(description, elementList, factor, additional);
    }

}
