package cn.nlte.science.units.length;

import cn.nlte.science.units.BasicUnit;
import cn.nlte.science.units.DerivedUnit;
import cn.nlte.science.units.UnitElement;
import java.util.ArrayList;
import java.util.List;

/**
 * 长度单位：li，中国单位，1里=500米
 *
 * @author yetao
 */
public class ULength_li extends DerivedUnit {

    private final static List<UnitElement> elementList = new ArrayList<>();
    private final static double factor = 500.0;
    private final static double additional = 0.0;
    private final static String description = "li";

    static {
        elementList.add(new UnitElement(BasicUnit.m, 1.0));
    }

    public ULength_li() {
        super(description, elementList, factor, additional);
    }

}
