package cn.nlte.science.units.length;

import cn.nlte.science.units.BasicUnit;
import cn.nlte.science.units.DerivedUnit;
import cn.nlte.science.units.UnitElement;
import java.util.ArrayList;
import java.util.List;

/**
 * 长度单位：yd，码，1码=36英寸
 *
 * @author yetao
 */
public class ULength_yd extends DerivedUnit {

    private final static List<UnitElement> elementList = new ArrayList<>();
    private final static double factor;
    private final static double additional = 0.0;
    private final static String description = "yd";

    static {
        // 根据已有的初始化
        factor = new ULength_in().getFactor() * 36.0;
        elementList.add(new UnitElement(BasicUnit.m, 1.0));
    }

    public ULength_yd() {
        super(description, elementList, factor, additional);
    }

}
