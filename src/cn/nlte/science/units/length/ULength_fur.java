package cn.nlte.science.units.length;

import cn.nlte.science.units.BasicUnit;
import cn.nlte.science.units.DerivedUnit;
import cn.nlte.science.units.UnitElement;
import java.util.ArrayList;
import java.util.List;

/**
 * 长度单位：fur，弗隆，1弗隆=0.125英里
 *
 * @author yetao
 */
public class ULength_fur extends DerivedUnit {

    private final static List<UnitElement> elementList = new ArrayList<>();
    private final static double factor;
    private final static double additional = 0.0;
    private final static String description = "fur";

    static {
        factor = new ULength_mi().getFactor() * 0.125;
        elementList.add(new UnitElement(BasicUnit.m, 1.0));
    }

    public ULength_fur() {
        super(description, elementList, factor, additional);
    }

}
