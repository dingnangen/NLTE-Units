package cn.nlte.science.units.length;

import cn.nlte.science.units.BasicUnit;
import cn.nlte.science.units.DerivedUnit;
import cn.nlte.science.units.UnitElement;
import java.util.ArrayList;
import java.util.List;

/**
 * 长度单位：fen，中国单位，1分=0.01尺
 *
 * @author yetao
 */
public class ULength_fen extends DerivedUnit {

    private final static List<UnitElement> elementList = new ArrayList<>();
    private final static double factor;
    private final static double additional = 0.0;
    private final static String description = "fen";

    static {
        factor = new ULength_chi().getFactor() * 0.01;
        elementList.add(new UnitElement(BasicUnit.m, 1.0));
    }

    public ULength_fen() {
        super(description, elementList, factor, additional);
    }

}
