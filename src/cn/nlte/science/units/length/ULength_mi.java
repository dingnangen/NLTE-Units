package cn.nlte.science.units.length;

import cn.nlte.science.units.BasicUnit;
import cn.nlte.science.units.DerivedUnit;
import cn.nlte.science.units.UnitElement;
import java.util.ArrayList;
import java.util.List;

/**
 * 长度单位：mi，英里，1英里=5280英尺
 *
 * @author yetao
 */
public class ULength_mi extends DerivedUnit {

    private final static List<UnitElement> elementList = new ArrayList<>();
    private final static double factor;
    private final static double additional = 0.0;
    private final static String description = "mi";

    static {
        // 根据已有的初始化
        factor = new ULength_ft().getFactor() * 5280.0;
        elementList.add(new UnitElement(BasicUnit.m, 1.0));
    }

    public ULength_mi() {
        super(description, elementList, factor, additional);
    }

}
