package cn.nlte.science.units.power;

import cn.nlte.science.units.BasicUnit;
import cn.nlte.science.units.DerivedUnit;
import cn.nlte.science.units.UnitElement;
import cn.nlte.science.units.ValueWithUnit;
import cn.nlte.science.units.force.UForce_lbf;
import cn.nlte.science.units.length.ULength_ft;
import cn.nlte.science.units.time.UTime_s;
import java.util.ArrayList;
import java.util.List;

/**
 * 功率单位：Hp，英制马力
 *
 * @author yetao
 */
public class UPower_Hp extends DerivedUnit {

    private final static List<UnitElement> elementList = new ArrayList<>();
    private final static double factor;
    private final static double additional = 0.0;
    private final static String description = "Hp";

    static {
        ValueWithUnit vwu = new ValueWithUnit(1.0, new ULength_ft());
        factor = vwu.multiply(new ValueWithUnit(1.0, new UForce_lbf())).divide(new ValueWithUnit(1.0, new UTime_s())).getBasicValue() * 550.0;
        elementList.add(new UnitElement(BasicUnit.Kg, 1.0));
        elementList.add(new UnitElement(BasicUnit.m, 2.0));
        elementList.add(new UnitElement(BasicUnit.s, -3.0));
    }

    public UPower_Hp() {
        super(description, elementList, factor, additional);
    }

}
