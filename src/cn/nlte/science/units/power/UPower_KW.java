package cn.nlte.science.units.power;

import cn.nlte.science.units.BasicUnit;
import cn.nlte.science.units.DerivedUnit;
import cn.nlte.science.units.UnitElement;
import java.util.ArrayList;
import java.util.List;

/**
 * ���ʵ�λ��KW
 *
 * @author yetao
 */
public class UPower_KW extends DerivedUnit {

    private final static List<UnitElement> elementList = new ArrayList<>();
    private final static double factor = 1000.0;
    private final static double additional = 0.0;
    private final static String description = "KW";

    static {
        elementList.add(new UnitElement(BasicUnit.Kg, 1.0));
        elementList.add(new UnitElement(BasicUnit.m, 2.0));
        elementList.add(new UnitElement(BasicUnit.s, -3.0));
    }

    public UPower_KW() {
        super(description, elementList, factor, additional);
    }

}
