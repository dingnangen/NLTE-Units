package cn.nlte.science.units.mass;

import cn.nlte.science.units.BasicUnit;
import cn.nlte.science.units.DerivedUnit;
import cn.nlte.science.units.UnitElement;
import java.util.ArrayList;
import java.util.List;

/**
 * 质量单位：Kg，千克
 *
 * @author yetao
 */
public class UMass_Kg extends DerivedUnit {

    private final static List<UnitElement> elementList = new ArrayList<>();
    private final static double factor = 1.0;
    private final static double additional = 0.0;
    private final static String description = "Kg";

    static {
        elementList.add(new UnitElement(BasicUnit.Kg, 1.0));
    }

    public UMass_Kg() {
        super(description, elementList, factor, additional);
    }

}
