package cn.nlte.science.units.energy;

import cn.nlte.science.units.BasicUnit;
import cn.nlte.science.units.DerivedUnit;
import cn.nlte.science.units.UnitElement;
import java.util.ArrayList;
import java.util.List;

/**
 * ������λ��cal����
 *
 * @author yetao
 */
public class UEnergy_cal extends DerivedUnit {

    private final static List<UnitElement> elementList = new ArrayList<>();
    private final static double factor = 4.185851820846;
    private final static double additional = 0.0;
    private final static String description = "cal";

    static {
        elementList.add(new UnitElement(BasicUnit.Kg, 1.0));
        elementList.add(new UnitElement(BasicUnit.m, 2.0));
        elementList.add(new UnitElement(BasicUnit.s, -2.0));
    }

    public UEnergy_cal() {
        super(description, elementList, factor, additional);
    }

}
