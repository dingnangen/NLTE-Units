package cn.nlte.science.units.energy;

import cn.nlte.science.units.BasicUnit;
import cn.nlte.science.units.DerivedUnit;
import cn.nlte.science.units.UnitElement;
import cn.nlte.science.units.ValueWithUnit;
import cn.nlte.science.units.force.UForce_lbf;
import cn.nlte.science.units.length.ULength_ft;
import java.util.ArrayList;
import java.util.List;

/**
 * ������λ��ft*lbf
 *
 * @author yetao
 */
public class UEnergy_ftlbf extends DerivedUnit {

    private final static List<UnitElement> elementList = new ArrayList<>();
    private final static double factor;
    private final static double additional = 0.0;
    private final static String description = "ft*lbf";

    static {
        ValueWithUnit vwu = new ValueWithUnit(1.0, new ULength_ft());
        factor = vwu.multiply(new ValueWithUnit(1.0, new UForce_lbf())).getBasicValue();
        elementList.add(new UnitElement(BasicUnit.Kg, 1.0));
        elementList.add(new UnitElement(BasicUnit.m, 2.0));
        elementList.add(new UnitElement(BasicUnit.s, -2.0));
    }

    public UEnergy_ftlbf() {
        super(description, elementList, factor, additional);
    }

}
