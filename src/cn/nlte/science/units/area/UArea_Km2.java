package cn.nlte.science.units.area;

import cn.nlte.science.units.BasicUnit;
import cn.nlte.science.units.DerivedUnit;
import cn.nlte.science.units.UnitElement;
import cn.nlte.science.units.length.ULength_Km;
import java.util.ArrayList;
import java.util.List;

/**
 * 面积单位：Km2，平方公里
 *
 * @author yetao
 */
public class UArea_Km2 extends DerivedUnit {

    private final static List<UnitElement> elementList = new ArrayList<>();
    private final static double factor;
    private final static double additional = 0.0;
    private final static String description = "Km2";

    static {
        factor = Math.pow(new ULength_Km().getFactor(), 2.0);
        elementList.add(new UnitElement(BasicUnit.m, 2.0));
    }

    public UArea_Km2() {
        super(description, elementList, factor, additional);
    }

}
