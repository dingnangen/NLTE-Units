package cn.nlte.science.units;

/**
 * 本类提供本包中特定的数学操作
 *
 * @author yetao
 */
public class DoubleUtils {

    public static final double DEVIATION = 1E-8;

    /**
     * 判断在指定误差条件下，两浮点数是否相等
     *
     * @param num1
     * @param num2
     * @return
     */
    public static boolean isSame(double num1, double num2) {
        return Math.abs(num1 - num2) <= DEVIATION;
    }

}
