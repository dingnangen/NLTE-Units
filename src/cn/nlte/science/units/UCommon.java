package cn.nlte.science.units;

import java.util.List;

/**
 * 封装DerivedUnit类的通用实例
 *
 * @author yetao
 */
public class UCommon extends DerivedUnit {

    public UCommon(String description, List<UnitElement> units, double factor, double addition) {
        super(description, units, factor, addition);
    }

}
