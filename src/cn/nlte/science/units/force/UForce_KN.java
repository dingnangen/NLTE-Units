package cn.nlte.science.units.force;

import cn.nlte.science.units.BasicUnit;
import cn.nlte.science.units.DerivedUnit;
import cn.nlte.science.units.UnitElement;
import java.util.ArrayList;
import java.util.List;

/**
 * ����λ��KN
 *
 * @author yetao
 */
public class UForce_KN extends DerivedUnit {

    private final static List<UnitElement> elementList = new ArrayList<>();
    private final static double factor = 1000.0;
    private final static double additional = 0.0;
    private final static String description = "KN";

    static {
        elementList.add(new UnitElement(BasicUnit.Kg, 1.0));
        elementList.add(new UnitElement(BasicUnit.m, 1.0));
        elementList.add(new UnitElement(BasicUnit.s, -2.0));
    }

    public UForce_KN() {
        super(description, elementList, factor, additional);
    }

}
