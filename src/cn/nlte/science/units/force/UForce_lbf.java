package cn.nlte.science.units.force;

import cn.nlte.science.units.BasicUnit;
import cn.nlte.science.units.DerivedUnit;
import cn.nlte.science.units.UnitElement;
import cn.nlte.science.units.ValueWithUnit;
import cn.nlte.science.units.accelerate.UAccelerate_g;
import cn.nlte.science.units.mass.UMass_lb;
import java.util.ArrayList;
import java.util.List;

/**
 * 力单位：lbf，磅力
 *
 * @author yetao
 */
public class UForce_lbf extends DerivedUnit {

    private final static List<UnitElement> elementList = new ArrayList<>();
    private final static double factor;
    private final static double additional = 0.0;
    private final static String description = "lbf";

    static {
        ValueWithUnit vwu = new ValueWithUnit(1.0, new UMass_lb());
        // 中立加速度的值在基准单位制下是相同的，因此此处乘以标准重力加速度即可
        factor = vwu.multiply(new ValueWithUnit(1.0, new UAccelerate_g())).getBasicValue();
        elementList.add(new UnitElement(BasicUnit.Kg, 1.0));
        elementList.add(new UnitElement(BasicUnit.m, 1.0));
        elementList.add(new UnitElement(BasicUnit.s, -2.0));
    }

    public UForce_lbf() {
        super(description, elementList, factor, additional);
    }

}
