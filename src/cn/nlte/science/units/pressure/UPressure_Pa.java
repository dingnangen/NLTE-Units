package cn.nlte.science.units.pressure;

import cn.nlte.science.units.BasicUnit;
import cn.nlte.science.units.DerivedUnit;
import cn.nlte.science.units.UnitElement;
import java.util.ArrayList;
import java.util.List;

/**
 * ѹǿ��λ��Pa��1Pa=Kg/(m*s^2)
 *
 * @author yetao
 */
public class UPressure_Pa extends DerivedUnit {

    private final static List<UnitElement> elementList = new ArrayList<>();
    private final static double factor = 1.0;
    private final static double additional = 0.0;
    private final static String description = "Pa";

    static {
        elementList.add(new UnitElement(BasicUnit.Kg, 1.0));
        elementList.add(new UnitElement(BasicUnit.m, -1.0));
        elementList.add(new UnitElement(BasicUnit.s, -2.0));
    }

    public UPressure_Pa() {
        super(description, elementList, factor, additional);
    }

}
