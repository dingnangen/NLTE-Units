package cn.nlte.science.units.pressure;

import cn.nlte.science.units.BasicUnit;
import cn.nlte.science.units.DerivedUnit;
import cn.nlte.science.units.UnitElement;
import java.util.ArrayList;
import java.util.List;

/**
 * 压强单位：atm，标准大气压
 *
 * @author yetao
 */
public class UPressure_atm extends DerivedUnit {

    private final static List<UnitElement> elementList = new ArrayList<>();
    private final static double factor = 101325.0;
    private final static double additional = 0.0;
    private final static String description = "atm";

    static {
        elementList.add(new UnitElement(BasicUnit.Kg, 1.0));
        elementList.add(new UnitElement(BasicUnit.m, -1.0));
        elementList.add(new UnitElement(BasicUnit.s, -2.0));
    }

    public UPressure_atm() {
        super(description, elementList, factor, additional);
    }

}
