package cn.nlte.science.units.pressure;

import cn.nlte.science.units.BasicUnit;
import cn.nlte.science.units.DerivedUnit;
import cn.nlte.science.units.UnitElement;
import java.util.ArrayList;
import java.util.List;

/**
 * ѹǿ��λ��MPa
 *
 * @author yetao
 */
public class UPressure_MPa extends DerivedUnit {

    private final static List<UnitElement> elementList = new ArrayList<>();
    private final static double factor = 1.0E6;
    private final static double additional = 0.0;
    private final static String description = "MPa";

    static {
        elementList.add(new UnitElement(BasicUnit.Kg, 1.0));
        elementList.add(new UnitElement(BasicUnit.m, -1.0));
        elementList.add(new UnitElement(BasicUnit.s, -2.0));
    }

    public UPressure_MPa() {
        super(description, elementList, factor, additional);
    }

}
